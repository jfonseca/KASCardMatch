// import { chunk, range } from "lodash";
const config = require("./config");
const cards = require("./cards");
const restify = require("restify");
const _ = require("lodash");

///DATABASE
const MongoClient = require("mongodb").MongoClient;

MongoClient.connect(config.db.uri, function(err, db) {
  if (err) throw err;
  console.log("Database created!");
  let dbo = db.db("api");
  dbo.createCollection("decks", function(err, res) {
    if (err) throw err;
    console.log("Collection created!");
    db.close();
  });
});

//START SERVER
const server = restify.createServer({
  name: config.name,
  version: config.version
});

server.listen(config.port, () => {
  console.log(`Server is listening on port ${config.port}`);
});

server.pre((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Headers', "Content-Type");
  next();
})

server.opts(/\.*/, function (req, res, next) {
  //console.log('req   ', req);
  res.send(200);
  next();
});

server.use(restify.plugins.bodyParser());

/**
 * ADD NEW DECK TO DB AND RETURN
 */
server.post("/deck", (req, res, next) => {

  //GET NEW DECK TO ADD TO DB
  data = cards.shuffle();

  MongoClient.connect(config.db.uri, function(err, db) {
    if (err) next(err);

    let dbo = db.db("api");
    data.createdAt = new Date();
    dbo.collection("decks").insertOne(data, function(err, result) {
      if (err) next(err);
      //console.log("1 document inserted", result.ops[0]);
      res.send(result.ops[0]);
      db.close();
      // next(result);
    });
  });
});

/**
 * GET 5 DECKS FROM DB
 */
server.get("/deck", (req, res, next) => {
  MongoClient.connect(config.db.uri, function(err, db) {
    if (err) next(err);

    let dbo = db.db("api");
    let cursor = dbo
      .collection("decks")
      .find({})
      .sort({$natural:-1})
      .toArray(function(err, result) {
        if (err) next(err);
        console.log(result);
        res.send(result);
        db.close();
        // next(result);
      });
  });
});