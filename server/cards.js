const _ = require("lodash");

const names = ["A", "2", "3", "4", "5", "6", "7", "8", "9", "T", "J", "Q", "K"];
const suits = ["Spades", "Diamonds", "Hearts", "Clubs"];
let cardArr = [];

function shuffle() {
  let dbItem = {};
  let total = 0;

  cardArr = _.chain(deck())
    .shuffle()
    .chunk(13)
    .value();

  return checkValue();
  
}

function deck() {
  const cards = [];

  suits.forEach(suit => {
    names.forEach(name => cards.push({ name: name, suit: suit }));
  });
  return cards;
}

function setPair(arr, pair) {
  // CHECKS FOR PAIRS AND ADDS VALUE TO OBJECT
  cardArr.forEach((suit, i) => {
    suit.forEach((card, j) => {
      arr.forEach(item => {
        if (item.posX == j && item.posY == i && !cardArr[i][j].pair) {
          cardArr[i][j].pair = pair;
        }
      });
    });
  });
}

function searchMatches(item, key, amt) {
  // Check selected item in grid
  let x = [-1, 0, 1, -1, 1, -1, 0, 1];
  let y = [-1, -1, -1, 0, 0, 1, 1, 1];

  //
  for (dir = 0; dir < 8; dir++) {
    let yDir = item.posY + y[dir],
      xDir = item.posX + x[dir];

    let tempArr = [];

    for (let k = 1; k < amt; k++) {
      // Check distance is within bounds
      if (
        yDir >= cardArr.length ||
        yDir < 0 ||
        xDir >= cardArr[0].length ||
        xDir < 0
      )
        break;

      let tempItem = cardArr[yDir][xDir];

      // check key of matches
      if (tempItem[key] != item[key]) break;

      tempArr.push(tempItem);

      //Update next position check
      (yDir += y[dir]), (xDir += x[dir]);
    }

    if (tempArr.length == amt - 1) {
      tempArr.push(item);
      return tempArr;
    }
  }
  return [];
}

function checkValue() {
  let cardAmt = [4, 3, 2];
  let dbObj = {};
  let pairArr = [];
  let total = 0;

  cardAmt.forEach((amt, i) => {
    pairArr[i] = [];
    total = 0;
    cardArr.forEach((suit, y) => {
      suit.forEach((card, x) => {
        let val = 0;
        val += card.name == names[x] ? 1 : 0;
        val += card.suit == suits[y] ? 1 : 0;
        total += val;
        card.value = val;
        card.file = card.suit + "_" + card.name + ".png";

        (card.posX = x), (card.posY = y);
        let itemMatch = searchMatches(card, "name", amt);
        if (itemMatch.length > 0) {
          pairArr[i] = pairArr[i].concat(itemMatch);
        }
      });
    });
    setPair(pairArr[i], amt);
  });
  dbObj.total = total;
  dbObj.cards = cardArr;
  return dbObj;
}
module.exports = {
    shuffle:shuffle
}
