# KAS CARD MATCH

Dealer shuffles 52 cards and adds them to the table. If a card matches either the Suit (♠, ♦, ♥, ♣) or the number (1-13) it will receive 1 point per each match. The max points per card is 2 and if all cards on table match the highest score is 104. This project uses React, Mongodb, node, and restify

## Example

To run the examples locally please follow the steps below.

[NPM](https://www.npmjs.com/get-npm) - Install NPM if you currently don't have it


* Install the package from npm

`npm install` and `npm install --only=dev`


* Run on local machine

`npm start`


### Features:

* Match Pair, Triplet, and Quad Checker : The table checks for all cards that match diagonal horizontal, and vertical
* Css : Apply class to matches
* Display of current score and average of all decks in database
* Allowed to view previous decks

### Folder Structure:

-src : Main folder this is a basic react load

-server : This is the folder with the API, and Card shuffle logic

--File: cards.js : This is used to make a new deck of shuffled cards and check for all matches and points

-data : Stores all the data from Mongodb

-public : Is main folder used while running

### Component Highlight:

* MongoDB : Database storage
* restify : API handles all the communication between db and frontend
* axios : Used to make API calls
* lodash : Makes many task easier

## Authors

* **Jose Fonseca** - _[Klip Art Studio](https://www.klipartstudio.com)_ - [GIT](https://gitlab.com/jfonseca/KASReactCalendar.git)

## License

MIT

![calendar](screenshots/App.png)