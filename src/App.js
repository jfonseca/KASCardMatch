import React, { Component } from "react";
import axios from "axios";

import "./App.css";

class App extends Component {
  constructor(props) {
    super(props);

    this.suitIcons = ["♠", "♦", "♥", "♣"];
    this.names = ["A", "2", "3", "4", "5", "6", "7", "8", "9", "T", "J", "Q", "K"];
    this.state = {
      history: [],
      showTable: 0,
      total: 0,
      current: { cards: [], total:0 },
      average: 0
    };

    this.base_url = "http://localhost:3000";

    this.axiosConfig = {
      headers: {
        "Content-Type": "application/json;charset=UTF-8",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "*",
        "Access-Control-Allow-Methods": "PUT, GET, POST, DELETE, OPTIONS"
      }
    };
  }

  changeDeck = (num = 1) => {
    //Change deck on table and check if reached limits
    let tempDeck = this.state.showTable
    tempDeck += num;
    tempDeck = tempDeck < 0 ? 0 : tempDeck;
    tempDeck = (tempDeck > this.state.history.length - 1) ? this.state.history.length - 1 : tempDeck;
    this.setState({ showTable: tempDeck });
    this.updateTable();
  };

  newDeck = () => {
    //Make a new deck add it to the DB and return it
    axios
      .post(this.base_url + "/deck", this.axiosConfig)
      .then(response => {
        let tempArr = [].concat(this.state.history);
        tempArr.unshift(response.data);
        
        this.setState({
          history: tempArr,
          current: response.data
        });

        //0 is the last deck added to the table
        this.setState({ showTable: 0 });
        
        this.updateTable();
      })
      .catch(function(error) {
        console.log("error", error);
      });
  };

  updateTable = () => {
    let item = this.state.history[this.state.showTable];
    let getAverage = (this.state.history.reduce((avg, item) => avg + item.total, 0) / this.state.history.length).toFixed(2);
    this.setState({
      current: item,
      total: (item.total * (100 / 104)).toFixed(2) + "%",
      average: getAverage
    });
  };

  init = () => {
    //Load all decks from DB
    axios
      .get(this.base_url + "/deck")
      .then(response => {
        console.log("response", response);

        //Add decks to history
        this.setState({ history: response.data });

        this.updateTable();
      })
      .catch(function(error) {
        console.log("error", error);
      });
  };

  componentDidMount() {
    this.init();
  }
  render() {

    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">{"KAS CARD MATCH DECK "+ Math.abs(this.state.showTable-this.state.history.length)}</h1>
          <h2 className="App-score">{"Current Score is: " + this.state.current.total + " of 104 | " + this.state.total}</h2>
          <h3 className="App-average">{"Average Score: " + this.state.average + " from " + this.state.history.length + " decks "}</h3>
          <div className="sets"><span className='match2'>PAIRS</span><span className='match3'>TRIPLETS</span><span className='match4'>QUADS</span></div>
          <div className="nav">
            <button onClick={this.newDeck} className="dealBut">Deal Again</button>
            <button onClick={this.changeDeck.bind(this, -1)} className="arrows" disabled={this.state.showTable <= 0 ? true : false }>{">"}</button>
            <button onClick={this.changeDeck.bind(this, 1)} className="arrows" disabled={this.state.showTable >= (this.state.history.length-1) ? true : false }>{"<"}</button>
          </div>
        </header>
        <section id="table">
        <div className="tableHead">
        <div className="suit">&nbsp;</div>
        {this.names.map(barId =>
          <div key={barId} className="barCard">{barId}</div>
        )}
        </div>
          {this.state.current.cards.map((suit, id) => (
            <div key={id} className="tableRow">
            <div className="suit">{this.suitIcons[id]}</div>
              {suit.map((card, j) => (
                <div key={j} className={ "card match" + (card.pair !== undefined ? card.pair + " animate" : "0") }>
                  <img src={"/images/" + card.file} alt="" />
                </div>
              ))}
            </div>
          ))}
        </section>
        { (this.state.history.length == 0) ? <div className="nodecks" onClick={this.newDeck} >No decks available! <br />Please click here to deal your first deck</div> : ""}
        <div className="App-footer">
        <p>Learn More about this on Git</p>
        <a href='https://gitlab.com/jfonseca/KASCardMatch'><p>https://gitlab.com/jfonseca/KASCardMatch</p></a>
        </div>
      </div>
    );
  }
}

export default App;
